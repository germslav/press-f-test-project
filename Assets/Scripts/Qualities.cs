public interface IInteractive
{
    public void Interact();
}

public interface IPickable
{
    public void PickUp();
}