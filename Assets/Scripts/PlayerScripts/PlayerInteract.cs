using UnityEngine;
using StarterAssets;

[RequireComponent(typeof(StarterAssetsInputs))]  
public class PlayerInteract : MonoBehaviour
{
    [SerializeField]
    private Camera playerCamera;
    [SerializeField]
    private LayerMask interactedMask;

    private StarterAssetsInputs playerInput;
    private PlayerItems playerItems;

    private void Awake()
    {
        playerInput = GetComponent<StarterAssetsInputs>();
        playerItems = GetComponent<PlayerItems>();
    }

    private void Update()
    {
        if(playerInput.interact)
        {
            playerInput.interact = false;

            RaycastHit hit;

            Vector3 rayStartPos = playerCamera.transform.position;
            Vector3 rayEndPos = playerCamera.transform.position;

            if(Physics.CapsuleCast(rayStartPos, rayEndPos, 1f, playerCamera.transform.forward, out hit, 2f, interactedMask))
            {
                TryInteract(hit.collider.gameObject);
            }
        }
    }

    private void TryInteract(GameObject interactableObj)
    {
        if(interactableObj == null)
        {
            return;
        }

        IInteractive interactiveObj = interactableObj.GetComponent<IInteractive>();

        if(interactiveObj != null)
        {
            interactiveObj.Interact();
        }

        playerItems.TryPickUpItem(interactableObj);

    }
}
