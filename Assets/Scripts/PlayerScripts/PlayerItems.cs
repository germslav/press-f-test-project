using UnityEngine;
using StarterAssets;


public class PlayerItems : MonoBehaviour
{
    [SerializeField]
    private Transform pickingPoint;

    private GameObject currentItem;
    private void Start()
    {
        currentItem = null;
    }

    public void TryPickUpItem(GameObject pickingItem)
    {
        IPickable pickableObj = pickingItem.GetComponent<IPickable>();

        if (currentItem != null || pickableObj == null)
        {
            return;
        }

        pickableObj.PickUp();
        pickingItem.transform.position = pickingPoint.position;
        pickingItem.transform.SetParent(pickingPoint);

        currentItem = pickingItem;
    }

    public GameObject GetPlayerItem()
    {
        return currentItem;
    }
}
