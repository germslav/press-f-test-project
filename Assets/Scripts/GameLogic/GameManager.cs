using StarterAssets;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Transform itemsContainer;
    [SerializeField]
    private Transform receiversContainer;

    private GameLogic gameLogic;
    private GameObject player;

    public delegate void OnGameOverHandler();
    public event OnGameOverHandler OnGameOver;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");


        ShapeReceiver[] shapeReceivers = receiversContainer.GetComponentsInChildren<ShapeReceiver>();
        int itemsCount = itemsContainer.GetComponentsInChildren<Shape>().Length;

        gameLogic = new GameLogic(itemsCount, shapeReceivers);

        gameLogic.OnGameOver += GameLogic_OnGameOver;
    }

    private void GameLogic_OnGameOver()
    {
        player.GetComponent<FirstPersonController>().enabled = false;
        OnGameOver?.Invoke();
    }

    private void OnDestroy()
    {
        gameLogic.OnGameOver -= GameLogic_OnGameOver;
    }
}
