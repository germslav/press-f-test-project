public class GameLogic
{
    private int maxItemsCount;
    private ShapeReceiver[] shapeReceivers;

    private int currentItemsCount;

    public delegate void OnGameOverHandler();
    public event OnGameOverHandler OnGameOver;

    public GameLogic(int maxItemsCount, ShapeReceiver[] shapeReceivers)
    {
        this.maxItemsCount = maxItemsCount;
        this.shapeReceivers = shapeReceivers;
        currentItemsCount = maxItemsCount;

        foreach (ShapeReceiver shapeReceiver in shapeReceivers)
        {
            shapeReceiver.OnReceiverShapeAdd += ShapeReceiver_OnReceiverShapeAdd;
        }
    }

    private void ShapeReceiver_OnReceiverShapeAdd(int shapeCount, Shape.ShapeType shapeType)
    {
        currentItemsCount--;
        if(currentItemsCount <= 0)
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        foreach (ShapeReceiver shapeReceiver in shapeReceivers)
        {
            shapeReceiver.OnReceiverShapeAdd -= ShapeReceiver_OnReceiverShapeAdd;
        }
        OnGameOver?.Invoke();
    }
}
