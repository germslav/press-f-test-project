using UnityEngine;
using UnityEngine.UI;

public class ShapeCounter : MonoBehaviour
{
    [SerializeField]
    private Shape.ShapeType shapeType;

    [SerializeField]
    private Text counterText;

    public Shape.ShapeType ShapeType { get { return shapeType; } }

    public void ChangeShapeCount(int count)
    {
        counterText.text = count.ToString();
    }
}

