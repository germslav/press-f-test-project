using UnityEngine;

public class UserInterfaceManager : MonoBehaviour
{
    [SerializeField]
    private GameObject hud;
    [SerializeField]
    private GameObject ScoreTable;

    [SerializeField]
    private GameManager gameManager;
    private void Start()
    {
        gameManager.OnGameOver += GameManager_OnGameOver;
    }

    private void GameManager_OnGameOver()
    {
        hud.SetActive(false);
        ScoreTable.SetActive(true);
    }
}
