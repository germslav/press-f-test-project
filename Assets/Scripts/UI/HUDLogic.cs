using UnityEngine;

public class HUDLogic : MonoBehaviour
{
    [SerializeField]
    private ShapeCounter[] shapeCounters;
    [SerializeField]
    private ShapeReceiver[] shapeReceivers;

    private void Start()
    {
        foreach(ShapeReceiver shapeReceiver in shapeReceivers)
        {
            shapeReceiver.OnReceiverShapeAdd += ShapeReceiver_OnReceiverShapeAdd;
        }
    }

    private void OnDestroy()
    {
        foreach (ShapeReceiver shapeReceiver in shapeReceivers)
        {
            shapeReceiver.OnReceiverShapeAdd -= ShapeReceiver_OnReceiverShapeAdd;
        }
    }


    private void ShapeReceiver_OnReceiverShapeAdd(int shapeCount, Shape.ShapeType shapeType)
    {
        foreach(ShapeCounter shapeCounter in shapeCounters)
        {
            if(shapeType != shapeCounter.ShapeType)
            {
                continue;
            }

            shapeCounter.ChangeShapeCount(shapeCount);
        }
    }
}

