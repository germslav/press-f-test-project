using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreTableLogic : MonoBehaviour
{
    [SerializeField]
    private ShapeCounter[] shapeCounters;
    [SerializeField]
    private ShapeReceiver[] shapeReceivers;

    private void OnEnable()
    {
        foreach(ShapeCounter shapeCounter in shapeCounters)
        {
            int shapeCount = 0;

            for(int i = 0; i < shapeReceivers.Length; i++)
            {
                ShapeReceiver shapeReceiver = shapeReceivers[i];
                if (shapeReceiver.ReceivedShape == shapeCounter.ShapeType)
                {
                    shapeCount += shapeReceiver.ReceivedShapeCount;
                }
            }
            shapeCounter.ChangeShapeCount(shapeCount);
        }
    }

    public void OnResetButtonPressed()
    {
        SceneManager.LoadScene(0);
    }
}
