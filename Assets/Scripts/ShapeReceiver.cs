using UnityEngine;

public class ShapeReceiver : MonoBehaviour, IInteractive
{
    [SerializeField]
    private Shape.ShapeType receivedShape;

    private PlayerItems playerItems;
    private int shapeCount;


    public int ReceivedShapeCount { get { return shapeCount; } }
    public Shape.ShapeType ReceivedShape { get { return receivedShape; } }

    public delegate void OnReceiverShapeAddHandler(int shapeCount, Shape.ShapeType shapeType);
    public event OnReceiverShapeAddHandler OnReceiverShapeAdd;

    private void Start()
    {
        playerItems = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerItems>();
        shapeCount = 0;
    }

    public void Interact()
    {

        GameObject playerItem = playerItems.GetPlayerItem();

        if (playerItem == null)
        {
            return;
        }
        Shape shape = playerItem.GetComponent<Shape>();

        if (shape.CurrentShapeType == receivedShape)
        {
            Destroy(playerItem);
            shapeCount++;
            OnReceiverShapeAdd?.Invoke(shapeCount, receivedShape);
        }
    }

}
