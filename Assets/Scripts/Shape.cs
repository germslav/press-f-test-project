using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Shape : MonoBehaviour, IPickable
{
    public enum ShapeType
    {
        Capsule,
        Sphere,
        Cube
    }

    [SerializeField]
    private ShapeType currentShapeType;

    private Rigidbody rb;

    public ShapeType CurrentShapeType
    {
        get
        {
            return currentShapeType;
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void PickUp()
    {
        rb.isKinematic = true;
        gameObject.layer = 0;
    }

}
